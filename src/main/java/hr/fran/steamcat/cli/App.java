/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.cli;

import hr.fran.steamcat.update.VersionUpdate;
import hr.fran.steamcat.update.VersionUpdateChecker;
import org.assertj.core.util.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Spec;

import java.io.PrintWriter;
import java.net.http.HttpClient;
import java.util.Optional;

import static java.net.http.HttpClient.Redirect.NORMAL;

@Command(
        name = "steamcat",
        description = "SteamCat",
        versionProvider = VersionProvider.class,
        mixinStandardHelpOptions = true
)
public class App implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    @Spec
    @SuppressWarnings("initialization.field.uninitialized")
    protected CommandSpec commandSpec;

    public static void main(String[] args) {
        App app = new App();
        int exitCode = app.execute(args);
        System.exit(exitCode);
    }

    protected int execute(String[] args) {
        CommandLine commandLine = new CommandLine(this)
                .setExecutionExceptionHandler(new PrintMessageExceptionHandler());
        return commandLine.execute(args);
    }

    @Override
    public void run() {
        checkForUpdates();

        // TODO implement command
        getOut().println("SteamCat command run");
    }

    private void checkForUpdates() {
        try {
            VersionUpdateChecker versionUpdateChecker = createVersionUpdateChecker();
            Optional<VersionUpdate> versionUpdate = versionUpdateChecker.checkForUpdate();
            versionUpdate.ifPresent(update -> getOut().printf("New version %s is available at %s%n%n",
                    update.getVersion(),
                    update.getUrl()
            ));
        } catch (Exception e) {
            log.warn("Error checking for version update", e);
            getOut().printf("Error checking for version update: %s%n%n", e.getMessage());
        }
    }

    @VisibleForTesting
    protected VersionUpdateChecker createVersionUpdateChecker() {
        VersionProvider versionProvider = new VersionProvider();
        HttpClient httpClient = HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .build();
        return new VersionUpdateChecker(versionProvider, httpClient);
    }

    protected PrintWriter getOut() {
        return commandSpec.commandLine().getOut();
    }

}
