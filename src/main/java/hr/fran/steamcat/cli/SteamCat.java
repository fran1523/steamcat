/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.cli;

import com.lukaspradel.steamapi.data.json.ownedgames.Game;
import hr.fran.steamcat.Configuration;
import hr.fran.steamcat.cache.GameCacheManager;
import hr.fran.steamcat.cache.GameInfo;
import hr.fran.steamcat.steam.SteamClient;
import hr.fran.steamcat.steam.model.GameDetails;
import hr.fran.steamcat.steam.model.LocalGameCategory;
import hr.fran.steamcat.steam.model.WebGameDetails;
import net.platinumdigitalgroup.jvdf.VDFNode;
import net.platinumdigitalgroup.jvdf.VDFParser;
import net.platinumdigitalgroup.jvdf.VDFWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.stream.Collectors;

public class SteamCat {

    private final Configuration config = Configuration.instance();

    public static void main(String[] args) {
        SteamCat steamCat = new SteamCat();
        steamCat.testSteamClient();
    }

    private void testSteamClient() {
        SteamClient steamClient = new SteamClient(Configuration.instance());
        GameCacheManager cacheManager = GameCacheManager.instance();

        Collection<Game> ownedGames = steamClient.getOwnedGames();
        ownedGames.stream()
                .limit(10)
                .forEach(game -> {
                    GameInfo gameInfo = cacheManager.getGameInfo(game.getAppid());
                    if (gameInfo != null) {
                        System.out.println(gameInfo.gameDetails().name() + " is cached");
                    } else {
                        GameDetails gameDetails = steamClient.getStorefrontGameDetails(game.getAppid());
                        WebGameDetails webGameDetails = steamClient.getWebStoreGameDetails(gameDetails.steamAppid());
                        gameInfo = new GameInfo(gameDetails, webGameDetails);
                        cacheManager.setGameInfo(game.getAppid(), gameInfo);
                        cacheManager.setGameInfo(Long.valueOf(gameDetails.steamAppid()), gameInfo);
                    }

                    System.out.printf("appId:%s, steamAppId:%s, tags:%s%n",
                            game.getAppid(),
                            gameInfo.gameDetails().steamAppid(),
                            gameInfo.webGameDetails().tags());


                });

        cacheManager.save();
    }

    private void testSteamCategories() {
        try {
            String file = "/home/fran/.steam/steam/userdata/165082436/7/remote/sharedconfig.vdf";

            String vdfString = Files.readString(Path.of(file));

            VDFNode vdfNode = new VDFParser().parse(vdfString);
            Set<Map.Entry<String, Object[]>> apps = vdfNode.getSubNode("UserRoamingConfigStore")
                    .getSubNode("Software")
                    .getSubNode("Valve")
                    .getSubNode("Steam")
                    .getSubNode("Apps")
                    .entrySet();

            Map<String, LocalGameCategory> mappedApps = apps.stream()
                    .collect(Collectors.toMap(e -> e.getKey(), e -> bindToApp(e)));


            SteamClient steamClient = new SteamClient(Configuration.instance());
            Collection<Game> ownedGames = steamClient.getOwnedGames();

            for (Game ownedGame : ownedGames) {
                if (mappedApps.containsKey(ownedGame.getAppid().toString())) {
                    LocalGameCategory localGameCategory = mappedApps.get(ownedGame.getAppid().toString());
                    if ("1".equals(localGameCategory.hidden)
                            || (localGameCategory.tags != null && !localGameCategory.tags.isEmpty())) {
                        System.out.printf("%s: %s %s%n",
                                ownedGame.getName(),
                                localGameCategory.hidden,
                                localGameCategory.tags
                        );
                    }
                }
            }

            String output = new VDFWriter().write(vdfNode, true);
            Files.writeString(Path.of("out.vdf"), output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private LocalGameCategory bindToApp(Map.Entry<String, Object[]> entry) {
        VDFNode app = (VDFNode) entry.getValue()[0];
        LocalGameCategory result = new LocalGameCategory();
        String hidden = app.getString("hidden");

        List<String> tags = List.of();
        if (app.containsKey("tags")) {
            NavigableMap<String, Object[]> tagsMap = app.getSubNode("tags").descendingMap();
            tags = tagsMap.values().stream()
                    .map(o -> Arrays.stream(o).findFirst())
                    .flatMap(o -> o.stream())
                    .filter(o -> o instanceof String)
                    .map(o -> (String) o)
                    .toList();
        }

        result.hidden = hidden;
        result.tags = tags;
        return result;
    }

}
