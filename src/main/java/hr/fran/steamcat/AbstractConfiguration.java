/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat;

import org.immutables.value.Value;
import org.immutables.value.Value.Lazy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.URI;
import java.net.http.HttpClient;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

@Value.Immutable(builder = false, singleton = true)
@Value.Style(instance = "instance", typeImmutable = "*")
public class AbstractConfiguration {

    private static final Path CONFIGURATION_PROPERTIES_FILE =
            Path.of(System.getProperty("user.home"), "steamcat.properties");

    public static final Path GAME_CACHE_FILE = Path.of(System.getProperty("user.home"), "steamcat.gamedb");

    private final Properties configurationProperties;

    protected AbstractConfiguration() {
        configurationProperties = new Properties();
        try (BufferedReader propertiesReader = Files.newBufferedReader(CONFIGURATION_PROPERTIES_FILE)) {
            if (Files.isRegularFile(CONFIGURATION_PROPERTIES_FILE)) {
                configurationProperties.load(propertiesReader);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Lazy
    public HttpClient getHttpClient() {
        CookieManager cookieManager = new CookieManager();
        addSteamAgeVerificationCookies(cookieManager);
        return HttpClient.newBuilder()
                .cookieHandler(cookieManager)
                .build();
    }

    @SuppressWarnings("expression.unparsable")
    private void addSteamAgeVerificationCookies(CookieManager cookieManager) {
        URI storeSteampoweredCom = URI.create("https://store.steampowered.com/");
        List.of(
                new HttpCookie("birthtime", "944002801"),
                new HttpCookie("lastagecheckage", "1-0-2000"),
                new HttpCookie("wants_mature_content", "1")
        ).forEach(cookie -> {
            cookie.setPath("/");
            cookieManager.getCookieStore().add(storeSteampoweredCom, cookie);
        });
    }

    @Lazy
    public String getSteamApiKey() {
        return configurationProperties.getProperty("steamApiKey", "");
    }

    @Lazy
    public String getSteamId() {
        return configurationProperties.getProperty("steamId", "");
    }

    public Path getGameCacheFile() {
        return GAME_CACHE_FILE;
    }
}
