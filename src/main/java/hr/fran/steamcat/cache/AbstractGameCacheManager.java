/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.cache;

import hr.fran.steamcat.Configuration;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.immutables.value.Value;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

@Value.Immutable(builder = false, singleton = true)
@Value.Style(instance = "instance", typeImmutable = "*")
public class AbstractGameCacheManager {

    private final Configuration config = Configuration.instance();

    @Value.Lazy
    protected Map<Long, GameInfo> getCache() {
        if (!Files.exists(config.getGameCacheFile())) {
            return new HashMap<>();
        }

        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(config.getGameCacheFile()))) {
            Map<Long, GameInfo> cache = (Map<Long, GameInfo>) ois.readObject();
            System.out.println(cache.size() + " games loaded from cache");
            return cache;
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public @Nullable GameInfo getGameInfo(Long appId) {
        return getCache().get(appId);
    }

    public void setGameInfo(Long appId, GameInfo gameInfo) {
        getCache().put(appId, gameInfo);
    }

    public void save() {
        try (ObjectOutputStream ois = new ObjectOutputStream(Files.newOutputStream(config.getGameCacheFile()))) {
            ois.writeObject(getCache());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
