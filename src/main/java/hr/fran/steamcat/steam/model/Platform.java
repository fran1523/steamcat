/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.steam.model;

public class Platform {

    public static final String LINUX = "linux";
    public static final String MAC = "mac";
    public static final String WINDOWS = "windows";

}
