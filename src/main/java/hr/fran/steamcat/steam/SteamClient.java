/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.steam;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukaspradel.steamapi.core.exception.SteamApiException;
import com.lukaspradel.steamapi.data.json.ownedgames.Game;
import com.lukaspradel.steamapi.data.json.ownedgames.GetOwnedGames;
import com.lukaspradel.steamapi.data.json.ownedgames.Response;
import com.lukaspradel.steamapi.webapi.client.SteamWebApiClient;
import com.lukaspradel.steamapi.webapi.client.SteamWebApiClient.SteamWebApiClientBuilder;
import com.lukaspradel.steamapi.webapi.request.GetOwnedGamesRequest;
import com.lukaspradel.steamapi.webapi.request.builders.SteamWebApiRequestFactory;
import hr.fran.steamcat.Configuration;
import hr.fran.steamcat.steam.model.GameDetails;
import hr.fran.steamcat.steam.model.GameDetailsResponse;
import hr.fran.steamcat.steam.model.WebGameDetails;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SteamClient {

    private static final String STOREFRONT_API_URL = "https://store.steampowered.com/api/appdetails?appids=%s";

    private static final String WEB_STORE_URL = "https://store.steampowered.com/app/%s";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final Configuration config;

    public SteamClient(Configuration config) {
        this.config = config;
    }

    public Collection<Game> getOwnedGames() {
        try {
            SteamWebApiClient client = new SteamWebApiClientBuilder(config.getSteamApiKey()).build();
            GetOwnedGamesRequest request = SteamWebApiRequestFactory.createGetOwnedGamesRequest(
                    config.getSteamId(),
                    false,
                    true,
                    List.of()
            );
            GetOwnedGames games = client.processRequest(request);

            return Optional.ofNullable(games)
                    .map(GetOwnedGames::getResponse)
                    .map(Response::getGames)
                    .orElse(List.of());
        } catch (SteamApiException e) {
            throw new SteamClientException("Failed to get owned games for steamId: " + config.getSteamId(), e);
        }
    }

    // Storefront API documentation: https://wiki.teamfortress.com/wiki/User:RJackson/StorefrontAPI
    public GameDetails getStorefrontGameDetails(Long appId) {
        try {
            // TODO steam_id might change and redirect, remember mapping
            URL gameDetailsUrl = new URL(STOREFRONT_API_URL.formatted(appId));
            Map<Long, GameDetailsResponse> appIdToGameDetailsResponse = MAPPER.readValue(
                    gameDetailsUrl,
                    new TypeReference<>() {
                    }
            );
            return Optional.ofNullable(appIdToGameDetailsResponse.get(appId))
                    .filter(GameDetailsResponse::success)
                    .map(GameDetailsResponse::data)
                    .orElseThrow(() -> new SteamClientException("No game details returned for " + gameDetailsUrl));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public WebGameDetails getWebStoreGameDetails(String appId) {
        try {
            String gameDetailsUrl = WEB_STORE_URL.formatted(appId);

            HttpClient client = config.getHttpClient();
            HttpRequest request = HttpRequest.newBuilder(URI.create(gameDetailsUrl)).build();
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

            Document doc = Jsoup.parse(response.body());
            List<String> tags = doc.select("a.app_tag").eachText();
            return new WebGameDetails(tags);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
