/*
 * Copyright (C) 2024 Fran
 *
 * This file is part of SteamCat.
 *
 * SteamCat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SteamCat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SteamCat. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.steamcat.cli;

import hr.fran.steamcat.update.VersionUpdate;
import hr.fran.steamcat.update.VersionUpdateChecker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;
import uk.org.webcompere.systemstubs.stream.SystemErrAndOut;
import uk.org.webcompere.systemstubs.stream.output.MultiplexOutput;
import uk.org.webcompere.systemstubs.stream.output.Output;
import uk.org.webcompere.systemstubs.stream.output.TapStream;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SystemStubsExtension.class)
class AppTest {

    @SystemStub
    private final SystemErrAndOut systemErrAndOut = new SystemErrAndOut(
            new MultiplexOutput(new TapStream(), Output.fromStream(System.out))
    );

    @Mock
    private VersionUpdateChecker versionUpdateChecker;

    private final App sut = new TestApp();

    @Test
    void should_return_zero_when_there_are_no_errors() {
        int result = sut.execute(new String[]{});

        assertThat(result).isZero();
        assertThat(systemErrAndOut.getText()).isEqualToIgnoringNewLines("SteamCat command run");
    }

    @Test
    void should_return_non_zero_when_there_are_errors() {
        int result = sut.execute(new String[]{"unexpectedParam"});

        assertThat(result).isNotZero();
    }

    @Test
    void should_print_message_when_there_is_a_version_update() throws Exception {
        String version = "v1.0.0";
        String url = "https://gitlab.com/user1234/appname/-/releases";
        when(versionUpdateChecker.checkForUpdate()).thenReturn(Optional.of(
                new VersionUpdate.Builder().version(version).url(url).build()
        ));

        sut.execute(new String[]{});

        assertThat(systemErrAndOut.getText()).startsWith(String.format("New version %s is available at %s",
                version, url));
    }

    @Test
    void should_print_error_message_when_version_update_check_fails() throws Exception {
        when(versionUpdateChecker.checkForUpdate()).thenThrow(new RuntimeException("Update check failed"));

        sut.execute(new String[]{});

        assertThat(systemErrAndOut.getText()).contains("Error checking for version update: Update check failed");
    }

    private class TestApp extends App {

        @Override
        protected VersionUpdateChecker createVersionUpdateChecker() {
            return versionUpdateChecker;
        }

    }

}
