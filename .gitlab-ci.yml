# This is the Gradle build system for JVM applications
# https://gradle.org/
# https://github.com/gradle/gradle

stages:
  - build
  - test
  - upload
  - release

.gradle_cached:
  image: eclipse-temurin:17-jdk-alpine
  before_script:
    - GRADLE_USER_HOME="$(pwd)/.gradle"
    - export GRADLE_USER_HOME
  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - .gradle
      - build

build:
  stage: build
  extends: .gradle_cached
  script: ./gradlew --build-cache assemble
  cache:
    - policy: push
  artifacts:
    paths:
      - "build/distributions/${CI_PROJECT_NAME}-*.zip"

test:
  stage: test
  extends: .gradle_cached
  script: ./gradlew check jacocoLogTestCoverage
  cache:
    - policy: pull
  artifacts:
    reports:
      junit:
        - "**/test-results/test/TEST-*.xml"
  coverage: "/    - Instruction Coverage: ([0-9.]+)%/"

owasp-dependency-check:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/docker-dependency-check:latest
    entrypoint: [ "" ]
  stage: test
  script:
    - >
      /usr/share/dependency-check/bin/dependency-check.sh
      --scan "./" --format ALL
      --project "$CI_PROJECT_NAME" --failOnCVSS 0
  allow_failure: true
  artifacts:
    when: always
    expose_as: OWASP Dependency Check Report
    paths:
      - ./dependency-check-report.html
      - ./dependency-check-report.json

sonarcloud-check:
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  stage: test
  extends: .gradle_cached
  script: ./gradlew sonar
  only:
    - merge_requests
    - main

upload:
  image: curlimages/curl:latest
  stage: upload
  rules:
    - if: "$CI_COMMIT_TAG"
  before_script:
    - VERSION=$(expr substr "$CI_COMMIT_TAG" 2 32)
    - export VERSION
  script:
    - >
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "build/distributions/${CI_PROJECT_NAME}-${VERSION}.zip"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${VERSION}/${CI_PROJECT_NAME}-${VERSION}.zip"

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  rules:
    - if: "$CI_COMMIT_TAG"
  script:
    - echo "Running the release job for ${CI_COMMIT_TAG}."
  release:
    tag_name: "$CI_COMMIT_TAG"
    name: "$CI_COMMIT_TAG"
    description: Release created using the release-cli.
    assets:
      links:
        - name: "${CI_PROJECT_NAME}-$(expr substr $CI_COMMIT_TAG 2 32).zip"
          url: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/$(expr substr $CI_COMMIT_TAG 2 32)/${CI_PROJECT_NAME}-$(expr substr $CI_COMMIT_TAG 2 32).zip"
          link_type: package
